## 炉石拔线助手
### 跳过战斗动画
在招募阶段倒计时结束后，点击炉石拔线助手蓝色图标进行拔线，图标变为红色。重连后图标变回蓝色，直接进入招募阶段。

![Image](https://gitee.com/parnado/HSDisconnector-Releases/raw/master/images/1.webp)

也可以在进入战斗界面后拔线。

![Image](https://gitee.com/parnado/HSDisconnector-Releases/raw/master/images/2.webp)
### 调整图标位置
按住Ctrl拖动图标可以调整图标位置。

![Image](https://gitee.com/parnado/HSDisconnector-Releases/raw/master/images/3.webp)
### 自动隐藏图标
右键单击图标，选择“自动隐藏”。在鼠标移出图标时，炉石拔线助手会自动隐藏。

![Image](https://gitee.com/parnado/HSDisconnector-Releases/raw/master/images/4.webp)